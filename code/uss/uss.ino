// Ultrasonic sensor
int echoPin = A4;
int trigPin = A5;

void setup() {
  Serial.begin(9600);
  pinMode(echoPin,INPUT);
  pinMode(trigPin,OUTPUT);
}

long getUS1(){
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  long duration = pulseIn(echoPin, HIGH);
  return duration * 0.017; // get distance in mm
}

void printJSON(int us1){ //Print all the sensor data to serial as JSON
  Serial.print("{\"us1\":");
  Serial.print(us1);
  Serial.println("}");
}

void loop() {

  while(!Serial.available()); //Wait until it is signaled that new data is needed
  while(Serial.available()) Serial.read(); //Read everything from serial

  int us1 = getUS1(); //Get distance from wall with ultrasonic sensor

  printJSON(us1);   //Print data to serial.
}

