import numpy
import cv2
import time

def LB(new_value):
	# make sure to write the new value into the global variable
	global L_B
	L_B = new_value
	return

def LG(new_value):
	# make sure to write the new value into the global variable
	global L_G
	L_G = new_value
	return

def LR(new_value):
	# make sure to write the new value into the global variable
	global L_R
	L_R = new_value
	return

def HB(new_value):
	# make sure to write the new value into the global variable
	global H_B
	H_B = new_value
	return

def HG(new_value):
	# make sure to write the new value into the global variable
	global H_G
	H_G = new_value
	return

def HR(new_value):
	# make sure to write the new value into the global variable
	global H_R
	H_R = new_value
	return

L_B = 17
L_G = 57
L_R = 163
H_B = 38
H_G = 235
H_R = 255

trackbar_value = 32

# open the camera
cap = cv2.VideoCapture(0)

cv2.namedWindow("Values")
cv2.createTrackbar("L_B", "Values", L_B, 255, LB)
cv2.createTrackbar("L_G", "Values", L_G, 255, LG)
cv2.createTrackbar("L_R", "Values", L_R, 255, LR)
cv2.createTrackbar("H_B", "Values", H_B, 255, HB)
cv2.createTrackbar("H_G", "Values", H_G, 255, HG)
cv2.createTrackbar("H_R", "Values", H_R, 255, HR)


#Blob stuff
blob_params = cv2.SimpleBlobDetector_Params()
blob_params.minDistBetweenBlobs = 10
blob_params.filterByColor = True
blob_params.blobColor = 255
blob_params.filterByArea = False
blob_params.filterByCircularity = False
blob_params.filterByConvexity = False
blob_params.filterByInertia = False
detector = cv2.SimpleBlobDetector_create(blob_params)


seconds = time.time()


while True:


	fps = 1.0 / (time.time() - seconds)
	seconds = time.time()

	#read the image from the camera
	ret, frame = cap.read()

	#Make frame narrow
	r = [len(frame) - 200, len(frame) - 190, 0, len(frame[0])]
	frame = frame[r[0]:r[1], r[2]:r[3]]

	#blur the image
	blur = cv2.blur(frame, (5, 5))

	#convert BGR to HSV
	hsv_frame = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)

	lowerLimits = numpy.array([21, 84, 118])
	upperLimits = numpy.array([78, 213, 196])

	#thresholding
	thresholded = cv2.inRange(hsv_frame, lowerLimits, upperLimits)

	#Morphological operations
	kernel = numpy.ones((5, 5), numpy.uint8)
	opening = cv2.morphologyEx(thresholded, cv2.MORPH_OPEN, kernel)

	#Detect blobs
	keypoints = detector.detect(opening)

	x = 0
	for i in keypoints:
		x = int(i.pt[0])
	print(x)

	cv2.imshow("Original", frame)
	cv2.imshow("Processed", opening)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

go.stop()
cap.release()
cv2.destroyAllWindows()




