#!/usr/bin/env python3
# coding=utf-8
import serial
import gopigo as go
import time
import cv2
import json
import os
import time
import numpy as np

prop = 0

# open the camera
cap = cv2.VideoCapture(0)

#Blob stuff
blob_params = cv2.SimpleBlobDetector_Params()
blob_params.minDistBetweenBlobs = 10
blob_params.filterByColor = True
blob_params.blobColor = 255
blob_params.filterByArea = False
blob_params.filterByCircularity = False
blob_params.filterByConvexity = False
blob_params.filterByInertia = False
detector = cv2.SimpleBlobDetector_create(blob_params)

print("Battery voltage: " + str(go.volt()))

ser = serial.Serial('/dev/ttyUSB0', 9600)

try:
	dist = -1

	# Make sure arduino is ready to send the data.
	print("Syncing serial...0%\r", end='')
	while ser.in_waiting == 0:
		ser.write("R".encode())
	print("Syncing serial...50%\r", end='')
	while ser.in_waiting > 0:
		ser.readline()
	print("Syncing serial...100%")


	while True:

		# Read the serial input to string
		ser.write("R".encode()) # Send something to the Arduino to indicate we're ready to get some data.
		serial_line = ser.readline().strip() # Read the sent data from serial.

		try:

			# Decode the received JSON data
			data = json.loads(serial_line.decode())
			# Extract the sensor values
			dist = data['us1']
		except Exception as e:  # Something went wrong extracting the JSON.
			dist = -1           # Handle the situation.
			print(e)
			pass


		if dist != -1: # If a JSON was correctly extracted, continue.


			#read the image from the camera
			ret, frame = cap.read()

			#Make frame narrow
			r = [len(frame) - 200, len(frame) - 190, 0, len(frame[0])]
			frame = frame[r[0]:r[1], r[2]:r[3]]

			#blur the image
			blur = cv2.blur(frame, (5, 5))

			#convert BGR to HSV
			hsv_frame = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)

			lowerLimits = np.array([5, 190, 80])
			upperLimits = np.array([16, 255, 243])

			#thresholding
			thresholded = cv2.inRange(hsv_frame, lowerLimits, upperLimits)

			#Morphological operations
			kernel = np.ones((5, 5), np.uint8)
			opening = cv2.morphologyEx(thresholded, cv2.MORPH_OPEN, kernel)

			#Detect blobs
			keypoints = detector.detect(opening)
			keskpunkt = 0
			# Print received to the console
			print("DIST: ", dist, "LOCATION", keskpunkt)
			# Line following logic goes here
			kp = 0.1055
			e = 0
			gospeed = 50 
			try:
				if dist > 500 or dist == 0:
					if len(keypoints) == 1:
						keskpunkt = keypoints[0].pt[0]
						e = 320 - keskpunkt
						Pout = int(kp * e)
						s1 = 70 + Pout
						s2 = 70 - Pout
						go.set_right_speed(0)
						go.set_left_speed(0)
						go.fwd()
				elif dist < 350 and dist != 0:
					if len(keypoints) == 1:
						e = 320 - keskpunkt
						Pout = int(kp * e)
						gospeed = dist / 5
						go.set_right_speed(gospeed + Pout)
						go.set_left_speed(gospeed - Pout)
						go.fwd()
				elif dist < 500 and dist != 0:
					if len(keypoints) == 0:
						go.set_right_speed(0)
						go.fwd()
			except ValueError:
				go.set_right_speed(0)
				go.fwd()
except KeyboardInterrupt:
    print("Serial closed, program finished")

finally:
    ser.close()
go.stop()
cap.release()
cv2.destroyAllWindows()

